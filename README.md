# README #

### What is this repository for? ###

ZhJFtpServ is a minimalist FTP server. It allows you to share your files over the network by running a single console command. Similar programs exist, namely Swiss File Knife and ftpdmin, but both of them have the same shortcoming � they do not support anything other than US-ASCII characters in file names. There is no even way to stick to a single code page in both of these programs. ZhJFtpServ is written in Java so it is inherently Unicode-enabled.

### How do I get set up? ###

You can get the freshest build in the Downloads section.

Type `java -jar ./ZhJFtpServ.jar .` in the command line to share the current directory for anonymous users in read only mode.

Use `--help` command line argument to get help.

Important note: ZhJFTPServ is still in early development. While it implements basic authentication (see `--user` and `--pw` arguments) and user input sanitation do not use it in insecure or public networks (e.g. the Internet).

Of course you have to have JRE installed and java in your path. Check that by typing `java -version` in console.

I test ZhJFtpServ against the following FTP clients:

* WinSCP
* FAR Manager
* Total Commander
* Windows 10 built-in FTP client
* Mozilla Firefox (currently you must set text encoding to Unicode manually for each page)
  * FTP disabled by default since Firefox 88, can be enabled in `about:config`
* Brave (does not work from at least v1.24.86)

### Who do I talk to? ###

Andrey Zhmakin
