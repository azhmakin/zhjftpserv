//
// Copyright (c) 2017 Andrey Zhmakin
//

package zhmakin.jftpserv;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


/**
 * Main class of ZhJFtpServ - command line argument processing, initialisation of individual sessions.
 * @author Andrey Zhmakin
 */
public class JFtpServer
{
    Credentials credentials;
    String root;
    private int port;
    Options options;

    PrintStream outputStream = System.out;

    private List<SessionHandler> activeSessionList = Collections.synchronizedList(new LinkedList<SessionHandler>());

    private boolean running;


    public JFtpServer(Credentials credentials, String root, int port, Options options)
    {
        this.credentials = credentials;
        this.credentials.server = this;
        this.root = root;
        this.port = port;
        this.options = options;
    }


    public static void main(String[] args)
        throws IOException
    {
        if (args.length == 0)
        {
            System.out.println("Use: ZhJFtpServ --help\n");
            return;
        }

        Boolean readWrite   = null;
        String  user        = null;
        String  password    = null;
        boolean anonymous   = false;
        String  root        = null;
        Integer port        = null;
        Options options     = new Options();

        for (String arg : args)
        {
            if (arg.equals("--help"))
            {
                printArgumentHelp();
                return;
            }
            else if (arg.startsWith("--user="))
            {
                user = arg.substring("--user=".length());
            }
            else if (arg.startsWith("--pw="))
            {
                password = arg.substring("--pw=".length());
            }
            else if (arg.equals("--anonymous"))
            {
                anonymous = true;
            }
            else if (arg.startsWith("--port="))
            {
                String portStr = arg.substring("--port=".length());

                try
                {
                    int portNumber = Integer.parseInt(portStr);

                    if (portNumber < 0 || portNumber > 65535)
                    {
                        throw new Exception("Port number out of range!");
                    }

                    port = portNumber;
                }
                catch (Throwable t)
                {
                    System.out.println("Illegal port number!");
                    System.exit(1);
                }
            }
            else if (arg.equals("--verbose"))
            {
                options.verbose = true;
            }
            else if (arg.equals("--ro"))
            {
                if (readWrite != null)
                {
                    System.out.println("RO and RW are incompatible!\n");
                    return;
                }

                readWrite = false;
            }
            else if (arg.equals("--rw"))
            {
                if (readWrite != null)
                {
                    System.out.println("RO and RW are incompatible!\n");
                    return;
                }

                readWrite = true;
            }
            else if (arg.equals("--single-user"))
            {
                options.singleUser = true;
            }
            else if (arg.equals("--one-use"))
            {
                options.oneUse = true;
            }
            else
            {
                if (root == null)
                {
                    root = arg;
                }
                else
                {
                    System.out.println("Invalid syntax. Use --help to get proper parameter description.");
                    return;
                }
            }
        }

        Credentials credentials = new Credentials();

        if (readWrite == null)
        {
            readWrite = false;
        }

        if (user != null && password != null)
        {
            credentials.addUser(user, password, readWrite);
        }

        if (user == null && password == null)
        {
            anonymous = true;
        }

        if (anonymous)
        {
            credentials.addUser("anonymous", null, false);
        }

        if (root == null)
        {
            root = new File(".").getCanonicalPath();
        }
        else
        {
            File dir = new File(root);

            if (!dir.exists())
            {
                System.out.println("Root directory \'" + root + "\' does not exist!");
                return;
            }

            if (!dir.isDirectory())
            {
                System.out.println("\'" + root + "\' is not a directory!");
                return;
            }

            root = dir.getCanonicalPath();
        }

        if (port == null)
        {
            port = 21;
        }

        JFtpServer server = new JFtpServer(credentials, root, port, options);
        server.runServer();
    }


    private final static String[][] ARGUMENT_DESCRIPTION =
        {
            {"--user=<username>", "Specifies username"},
            {"--pw=<password>", "Specifies password"},
            {"--anonymous", "Allows read-only anonymous access"},
            {"--port=<port-number>", "Specifies port (default is 21)"},
            {"--ro", "Read only access (Default)"},
            {"--rw", "Read-write access"},
            {"--single-user", "Allow only one user connected at any given moment"},
            {"--one-use", "Let one user to connect and exit when his session ends"},
            {"--verbose", "Verbose mode"},
            {"--help", "Prints this help and exits"}
        };


    private static void printArgumentHelp()
    {
        System.out.println("ZhJFtpServer v0.0.7-alpha");
        System.out.println("By Andrey Zhmakin");
        System.out.println("Command line arguments:");

        int maxLength = 0;

        for (String[] row : ARGUMENT_DESCRIPTION)
        {
            maxLength = Math.max(maxLength, row[0].length());
        }

        for (String[] row : ARGUMENT_DESCRIPTION)
        {
            System.out.print("  ");

            System.out.print(row[0]);

            for (int i = maxLength - row[0].length() + 2; --i >= 0; )
            {
                System.out.print(' ');
            }

            System.out.println(row[1]);
        }
    }


    public void stopServer()
    {
        if (this.running)
        {
            this.running = false;

            // Kill all active sessions

            while (!this.activeSessionList.isEmpty())
            {
                SessionHandler sessionHandler = this.activeSessionList.get(0);

                sessionHandler.close();
            }
        }
    }


    public void runServer()
    {
        if (!isPortAvailable(this.port))
        {
            throw new RuntimeException("Port not available: " + this.port);
        }

        try (ServerSocket serverSocket = new ServerSocket(this.port))
        {
            if (this.options.verbose)
            {
                this.outputStream.println("Exposing directory \'" + root + "\' on "
                    + "localhost"
                    + ":" + serverSocket.getLocalPort());
            }

            this.running = true;

            while (this.running)
            {
                Socket socket = serverSocket.accept();

                if (this.options.singleUser && !this.activeSessionList.isEmpty())
                {
                    socket.close();
                    continue;
                }

                Session session = new Session(this, socket);

                SessionHandler sessionHandler = new SessionHandler(session);

                Thread sessionThread = new Thread(sessionHandler);
                sessionHandler.setThread(sessionThread);

                sessionThread.start();

                if (this.options.oneUse)
                {
                    this.running = false;
                }
            }

        }
        catch (IOException e)
        {
            System.err.println(e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }
    }


    public PrintStream getOutputStream()
    {
        return this.outputStream;
    }


    public void setOutputStream(PrintStream outputStream)
    {
        this.outputStream = outputStream;
    }


    public static boolean isPortAvailable(int port)
    {
        if (port < 0 || port > 0xFFFF)
        {
            return false;
        }

        try (
                ServerSocket ss = new ServerSocket(port);
                DatagramSocket ds = new DatagramSocket(port);
            )
        {
            ss.setReuseAddress(true);
            ds.setReuseAddress(true);

            return true;
        }
        catch (IOException e)
        {
            return false;
        }
    }


    private class SessionHandler implements Runnable
    {
        Thread thread;
        Session session;

        public SessionHandler(Session session)
        {
            this.session = session;
        }

        public void setThread(Thread thread)
        {
            this.thread = thread;
        }

        public void close()
        {
            // TODO: Stop within session instead!
            this.thread.stop();
            JFtpServer.this.activeSessionList.remove(this);
        }

        @Override
        public void run()
        {
            JFtpServer.this.activeSessionList.add(this);
            this.session.run();
            JFtpServer.this.activeSessionList.remove(this);
        }
    }


    public static class Options
    {
        public boolean singleUser = false;
        public boolean oneUse     = false;
        public boolean verbose    = false;
    }
}
