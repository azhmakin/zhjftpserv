//
// Copyright (c) 2017 Andrey Zhmakin
//

package zhmakin.jftpserv;

import java.io.*;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;


/**
 * Implements a session with a client.
 * @author Andrey Zhmakin
 */
public class Session
    implements Runnable
{
    private JFtpServer server;
    private FileSystem fs;

    private Socket socket;
    private DataOutputStream commandOutStream;

    String username;

    private boolean running = true;
    private boolean authenticated = false;

    private String address;
    private int port;

    private String renameFrom;

    private ServerSocket passiveServer;


    public Session(JFtpServer server, Socket socket)
    {
        this.server = server;
        this.socket = socket;
    }


    @Override
    public void run()
    {
        try
        {
            this.startSession();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    private void startSession()
        throws IOException
    {
        // Accept incoming connection
        this.commandOutStream = new DataOutputStream(this.socket.getOutputStream());

        // Respond
        this.sendCommand("220 (ZhJFtpServ)");

        while (this.running)
        {
            DataInputStream in = new DataInputStream(this.socket.getInputStream());
            BufferedReader bufIn = new BufferedReader(new InputStreamReader(in));

            String command = bufIn.readLine();

            if (command == null)
            {
                break;
            }

            if (this.server.options.verbose)
            {
                this.server.outputStream.println("COMMAND : " + command);
            }

            if (startsWithIgnoreCase(command, "USER "))
            {
                Scanner scanner = new Scanner(command);

                scanner.next();

                this.username = scanner.next();

                if (this.server.credentials.validateUsername(this.username))
                {
                    this.authenticated = false;
                    this.sendCommand("331 Please specify the password");
                }
                else
                {
                    this.username = null;

                    // TODO: Better code?
                    this.sendCommand("500 Login failed");
                    //System.exit(-1);
                }
            }
            else if (startsWithIgnoreCase(command, "PASS "))
            {
                Scanner scanner = new Scanner(command);

                scanner.next();

                if (!scanner.hasNext())
                {
                    // TODO: Better code?
                    this.sendCommand("530 Login failed");
                    continue;
                }

                String password = scanner.next();

                if (this.server.credentials.validatePassword(this.username, password))
                {
                    this.fs = new FileSystem(this.server.credentials.getRoot(username));

                    this.authenticated = true;
                    this.sendCommand("230 Login success");
                }
                else
                {
                    // TODO: Better code?
                    this.sendCommand("530 Login failed");
                    //System.exit(-1);
                }
            }
            else if (command.equalsIgnoreCase("QUIT"))
            {
                this.sendCommand("221 Goodbye.");
                commandOutStream.close();
                this.authenticated = false;
                this.running = false;
            }
            else
            {
                if (this.authenticated)
                {
                    this.processPostAuthenticationCommand(command);
                }
                else
                {
                    this.sendCommand("530 Authentication required.");
                }
            }
        }

        if (this.server.options.verbose)
        {
            this.server.outputStream.println("Closing session!");
        }

        this.socket.close();
    }


    private void processPostAuthenticationCommand(String command)
        throws IOException
    {
        if (command.equalsIgnoreCase("SYST"))
        {
            this.sendCommand("215 UNIX emulated by ZhJFTPServ");
        }
        else if (command.equalsIgnoreCase("FEAT"))
        {
            this.sendCommand("211-Features:\r\n UTF8\r\n SIZE\r\n211 End");
        }
        else if (command.equalsIgnoreCase("PWD"))
        {
            this.sendCommand("257 \"",
                FileSystem.to_utf8(this.fs.getCurrentDirectory()),
                "\"");
        }
        else if (command.equalsIgnoreCase("CDUP"))
        {
            int code = this.fs.goUp();

            this.responseToChangeDir(code);
        }
        else if (startsWithIgnoreCase(command, "CWD "))
        {
            String dir = command.substring(4);

            int code = this.fs.changeDirectory(dir);

            this.responseToChangeDir(code);
        }
        else if (startsWithIgnoreCase(command, "TYPE "))
        {
            Scanner scanner = new Scanner(command);

            scanner.next();

            String type = scanner.next();

            // Type A or a for ASCII
            // E - EBCDIC
            // I - Image

            if ("A".equalsIgnoreCase(type) || "I".equalsIgnoreCase(type))
            {
                this.sendCommand("200 OK");
            }
            else
            {
                // TODO: Proper error code?
                this.sendCommand("500 Data type not supported!");
            }
        }
        else if (command.equalsIgnoreCase("PASV"))
        {
            this.passiveServer = new ServerSocket(0);

            this.sendCommand("227 Entering Passive Mode (",
                addressToString(this.socket.getLocalAddress(), this.passiveServer.getLocalPort()),
                ").");
        }
        else if (command.equalsIgnoreCase("EPSV"))
        {
            this.passiveServer = new ServerSocket(0);

            this.sendCommand("229 Entering extended passive mode (|||" + this.passiveServer.getLocalPort() + "|)");
        }
        else if (startsWithIgnoreCase(command, "PORT "))
        {
            if (this.parsePortCommand(command))
            {
                this.sendCommand("200 OK");
            }
            else
            {
                // TODO: Better error code!
                this.sendCommand("500 Failure!");
            }
        }
        else if (startsWithIgnoreCase(command, "LIST"))
        {
            this.sendCommand("150 Listing directory contents.");

            Socket socket;

            if (this.passiveServer != null)
            {
                socket = this.passiveServer.accept();
            }
            else
            {
                socket = new Socket(this.address, this.port);
            }

            DataOutputStream dataOut = new DataOutputStream(socket.getOutputStream());

            this.fs.listCurrentDir(dataOut);

            dataOut.close();
            socket.close();

            this.sendCommand("226 Directory listing complete.");
        }
        else if (startsWithIgnoreCase(command, "RETR "))
        {
            String filename = command.substring(5);

            this.sendCommand("150 Opening binary mode data connection...");

            Socket socket;

            if (this.passiveServer != null)
            {
                socket = this.passiveServer.accept();
            }
            else
            {
                socket = new Socket(this.address, this.port);
            }

            DataOutputStream dataOut = new DataOutputStream(socket.getOutputStream());

            boolean ok = this.fs.retrieveFile(filename, dataOut);

            socket.close();

            this.sendCommand(ok ? "226 File transfer complete." : "500 File transfer error!");
        }
        else if (startsWithIgnoreCase(command, "STOR "))
        {
            if (!this.hasWriteAccess())
            {
                this.sendWriteAccessDeniedCommand();
                return;
            }

            String filename = command.substring(5);

            this.sendCommand("150 Ready to receive binary file...");

            Socket socket;

            if (this.passiveServer != null)
            {
                socket = this.passiveServer.accept();
            }
            else
            {
                socket = new Socket(this.address, this.port);
            }

            DataInputStream dataIn = new DataInputStream(socket.getInputStream());

            this.fs.storeFile(filename, dataIn);

            socket.close();

            this.sendCommand("226 File transfer complete.");
        }
        else if (startsWithIgnoreCase(command, "RNFR "))
        {
            if (!this.hasWriteAccess())
            {
                this.sendWriteAccessDeniedCommand();
                return;
            }

            String arg = command.substring(5);

            if (this.fs.exists(arg))
            {
                this.renameFrom = arg;
                this.sendCommand("350 Specify destination");
            }
            else
            {
                this.sendCommand("550 File does not exist!");
            }
        }
        else if (startsWithIgnoreCase(command, "RNTO "))
        {
            if (!this.hasWriteAccess())
            {
                this.sendWriteAccessDeniedCommand();
                return;
            }

            if (this.renameFrom == null)
            {
                this.sendCommand("503 RNFR should be sent first!");
                return;
            }

            String arg = command.substring(5);

            if (this.fs.rename(this.renameFrom, arg) == 0)
            {
                this.sendCommand("200 OK");
                this.renameFrom = null;
            }
            else
            {
                this.sendCommand("500 Rename failed!");
            }
        }
        else if (startsWithIgnoreCase(command, "SIZE "))
        {
            String filename = command.substring(5);

            // TODO: Use the commented line below in Java 1.8
            // this.sendCommand("213 ", Long.toUnsignedString(this.fs.getSize(filename)));

            this.sendCommand("213 ", Long.toString(this.fs.getSize(filename)));
        }
        else if (startsWithIgnoreCase(command, "DELE "))
        {
            if (!this.hasWriteAccess())
            {
                this.sendWriteAccessDeniedCommand();
                return;
            }

            String filename = command.substring(5);

            if (this.fs.delete(filename))
            {
                this.sendCommand("200 OK");
            }
            else
            {
                this.sendCommand("500 Cannot delete file.");
            }
        }
        else if (startsWithIgnoreCase(command, "MKD "))
        {
            if (!this.hasWriteAccess())
            {
                this.sendWriteAccessDeniedCommand();
                return;
            }

            String dir = command.substring(4);

            if (this.fs.createDir(dir))
            {
                this.sendCommand("200 OK");
            }
            else
            {
                // TODO: Better code!
                this.sendCommand("500 Cannot create directory.");
            }
        }
        else if (startsWithIgnoreCase(command, "RMD "))
        {
            if (!this.hasWriteAccess())
            {
                this.sendWriteAccessDeniedCommand();
                return;
            }

            String dir = command.substring(4);

            if (this.fs.delete(dir))
            {
                this.sendCommand("200 OK");
            }
            else
            {
                // TODO: Better code!
                this.sendCommand("500 Cannot delete directory.");
            }
        }
        else if (startsWithIgnoreCase(command, "HELP "))
        {
            Scanner scanner = new Scanner(command);

            scanner.next();

            // Queried by Total Commander FTP Client
            String arg = scanner.next();

            if ("SITE".equalsIgnoreCase(arg))
            {
                // TODO: Do something?
                this.sendCommand("200 OK");
            }
            else
            {
                // TODO: Better code!
                this.sendCommand("500 Command not supported");
            }
        }
        else if (startsWithIgnoreCase(command, "OPTS"))
        {
            Scanner scanner = new Scanner(command);

            scanner.next();

            String option = scanner.next();

            if ("UTF8".equalsIgnoreCase(option))
            {
                String param = scanner.next();

                if ("ON".equalsIgnoreCase(param))
                {
                    this.sendCommand("200 OK");
                }
                else
                {
                    // TODO: Is that correct?
                    this.sendCommand("500 Command not supported");
                }
            }
            else
            {
                // TODO: Is that correct?
                this.sendCommand("500 Command not supported");
            }

            //this.sendCommand("200 OK");
        }
        else if (command.equalsIgnoreCase("NOOP"))
        {
            this.sendCommand("200 OK");
        }
        else if (startsWithIgnoreCase(command, "REST"))
        {
            Scanner scanner = new Scanner(command);

            scanner.next();

            try
            {
                long startPos = scanner.nextLong();

                this.fs.setStartPos(startPos);

                this.sendCommand("350 Start position set!");
            }
            catch (NoSuchElementException e)
            {
                this.sendCommand("500 Illegal argument!");
            }
        }
        else
        {
            this.sendCommand("500 Command not supported");
        }
    }


    private static boolean startsWithIgnoreCase(String string, String substring)
    {
        if (substring.length() > string.length())
        {
            return false;
        }

        return string.substring(0, substring.length()).equalsIgnoreCase(substring);
    }


    private void responseToChangeDir(int code)
    {
        switch (code)
        {
            case FileSystem.CWD_OK:
                this.sendCommand("200 OK");
                break;

            case FileSystem.CWD_NOT_A_DIRECTORY:
                this.sendCommand("550 Not a directory!");
                break;

            case FileSystem.CWD_DIRECTORY_DOES_NOT_EXIST:
                this.sendCommand("550 Directory does not exist!");
                break;

            default:
                this.sendCommand("550 CWD failed!");
        }
    }


    private boolean hasWriteAccess()
    {
        return this.server.credentials.hasWriteAccess(this);
    }


    private boolean parsePortCommand(String command)
    {
        try
        {
            Scanner scanner = new Scanner(command);

            // Omit 'PORT'
            scanner.next();

            String arg = scanner.next();

            Scanner s2 = new Scanner(arg);
            s2.useDelimiter("\\s*,\\s*");

            int o3 = s2.nextInt();
            int o2 = s2.nextInt();
            int o1 = s2.nextInt();
            int o0 = s2.nextInt();

            int port = (s2.nextInt() << 8) + s2.nextInt();

            String address = o3 + "." + o2 + "." + o1 + "." + o0;

            this.address = address;
            this.port = port;

            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }


    private boolean sendCommand(Object... chunks)
    {
        boolean result = true;

        if (this.server.options.verbose)
        {
            this.server.outputStream.print("RESPONSE: ");
        }

        try
        {
            for (Object chunk : chunks)
            {
                if (chunk instanceof String)
                {
                    this.commandOutStream.writeBytes((String) chunk);

                    if (this.server.options.verbose) this.server.outputStream.print((String) chunk);
                }
                else if (chunk instanceof byte[])
                {
                    this.commandOutStream.write((byte[]) chunk);

                    if (this.server.options.verbose) this.server.outputStream.write((byte[]) chunk);
                }
                else
                {
                    assert false : "Unsupported data written!";
                }
            }

            this.commandOutStream.writeByte('\r');
            this.commandOutStream.writeByte('\n');
            this.commandOutStream.flush();

            if (this.server.options.verbose) this.server.outputStream.println();
        }
        catch (Exception e)
        {
            result = false;
        }

        return result;
    }


    private void sendWriteAccessDeniedCommand()
    {
        // TODO: Better code!
        this.sendCommand("500 Write access denied!");
    }


    private static String addressToString(InetAddress address, int port)
    {
        if (!(address instanceof Inet4Address))
        {
            System.out.println("Not IP4 address: " + address.getClass().getName());
            return null;
        }

        Inet4Address addr4 = (Inet4Address) address;

        StringBuilder builder = new StringBuilder();

        for (byte b : addr4.getAddress())
        {
            // TODO: Use the commented line below in Java 1.8
            // builder.append(Integer.toUnsignedString(b & 0xFF));

            builder.append(Integer.toString(b & 0xFF));
            builder.append(',');
        }

        builder.append((port >> 8) & 0xFF);
        builder.append(',');

        builder.append(port & 0xFF);

        return builder.toString();
    }
}
