//
// Copyright (c) 2017 Andrey Zhmakin
//

package zhmakin.jftpserv;

import java.util.HashMap;
import java.util.Map;


/**
 * Credential management and authentication routines.
 * @author Andrey Zhmakin
 */
public class Credentials
{
    JFtpServer server;
    Map<String, UserData> userToPasswordMap = new HashMap<>();


    public boolean validateUsername(String username)
    {
        return this.userToPasswordMap.containsKey(username);
    }


    public boolean validatePassword(String username, String password)
    {
        UserData data = this.userToPasswordMap.get(username);

        if (data == null)
        {
            return false;
        }

        return data.password == null || data.password.equals(password);
    }


    public boolean hasWriteAccess(Session session)
    {
        UserData data = this.userToPasswordMap.get(session.username);

        if (data == null)
        {
            return false;
        }

        return data.readWrite;
    }


    public void addUser(String user, String password, boolean readWrite)
    {
        this.userToPasswordMap.put(user, new UserData(password, readWrite));
    }


    public String getRoot(String username)
    {
        UserData data = this.userToPasswordMap.get(username);

        if (data == null)
        {
            return null;
        }

        return data.root != null ? data.root : this.server.root;
    }


    private static class UserData
    {
        String password;
        boolean readWrite;
        String root;


        public UserData(String password, boolean readWrite)
        {
            this.password = password;
            this.readWrite = readWrite;
        }


        public UserData(String password, boolean readWrite, String root)
        {
            this.password = password;
            this.readWrite = readWrite;
            this.root = root;
        }
    }
}
