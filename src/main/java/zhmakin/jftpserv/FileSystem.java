//
// Copyright (c) 2017 Andrey Zhmakin
//

package zhmakin.jftpserv;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;


/**
 * Interface for file system operations.
 * @author Andrey Zhmakin
 */
public class FileSystem
{
    private String root;
    private String current;
    private long startPos;

    public final static Charset UTF8 = Charset.forName("UTF-8");

    private final static int BUFFER_SIZE = 4096;
    private final static DateFormat DATE_FORMAT = new SimpleDateFormat("MMM d yyyy");

    private final static boolean legacy;

    static
    {
        String javaVersion = System.getProperty("java.version");

        legacy = javaVersion.startsWith("1.7.") || javaVersion.equals("0");
    }


    public FileSystem(String root)
    {
        this.root = root.replaceAll("\\\\", "/");

        if (!this.root.endsWith("/"))
        {
            this.root += '/';
        }

        this.current = "";
    }


    public String getCurrentDirectory()
    {
        return "/" + this.current + (this.current.isEmpty() ? "" : "/");
    }


    private String getCurrentLocalDirectory()
    {
        return !this.current.isEmpty()
            ? (this.root + this.current)
            : this.root.substring(0, this.root.length() - 1);
    }


    public final static int CWD_OK = 0;
    public final static int CWD_DIRECTORY_DOES_NOT_EXIST = 1;
    public final static int CWD_NOT_A_DIRECTORY  = 2;


    public int goUp()
    {
        if (this.current.isEmpty())
        {
            return CWD_DIRECTORY_DOES_NOT_EXIST;
        }

        String t = new StringBuilder(this.current).reverse().toString();

        int pos = t.indexOf('/');

        if (pos == -1)
        {
            this.current = "";
            return CWD_OK;
        }

        t = t.substring(pos + 1);

        this.current = new StringBuilder(t).reverse().toString();

        assert !this.current.endsWith("/");

        return CWD_OK;
    }


    public int changeDirectory(String change)
    {
        return legacy ? changeDirectoryJ7(change) : changeDirectoryJ8(change);
    }


    private int changeDirectoryJ8(String change)
    {
        if (change == null || change.isEmpty())
        {
            return CWD_OK;
        }

        if ("..".equals(change))
        {
            return this.goUp();
        }

        Path rootPath = Paths.get(this.root).normalize();

        Path candidatePath = Paths.get(this.root).normalize();

        Path delta;

        if (change.startsWith("/"))
        {
            delta = Paths.get(change.substring(1));
        }
        else
        {
            if (!this.current.equals(""))
            {
                delta = Paths.get(this.current);
                delta = delta.resolve(Paths.get(change));
            }
            else
            {
                delta = Paths.get(change);
            }
        }

        delta = delta.normalize();

        // Do not allow to go any higher than root!
        if (delta.toString().startsWith(".."))
        {
            return CWD_DIRECTORY_DOES_NOT_EXIST;
        }

        candidatePath = candidatePath.resolve(delta).normalize();

        if (!candidatePath.startsWith(rootPath))
        {
            return CWD_DIRECTORY_DOES_NOT_EXIST;
        }

        File dir = new File(candidatePath.toUri());

        if (!dir.exists())
        {
            return CWD_DIRECTORY_DOES_NOT_EXIST;
        }

        if (!dir.isDirectory())
        {
            return CWD_NOT_A_DIRECTORY;
        }

        this.current = delta.normalize().toString().replaceAll("\\\\", "/");

        return CWD_OK;
    }


    private int changeDirectoryJ7(String change)
    {
        if (change == null || change.isEmpty())
        {
            return CWD_OK;
        }

        if ("..".equals(change))
        {
            return this.goUp();
        }

        // TODO: Sanitise input properly!
        if (change.contains(".."))
        {
            return 100;
        }

        String current;
        String t;

        if (change.startsWith("/"))
        {
            current = "";
            t = change.substring(1);
        }
        else
        {
            current = this.current;
            t = change;
        }

        String collected = "/";

        do
        {
            int pos = t.indexOf('/');

            String dir = (pos != -1) ? t.substring(0, pos) : t;

            if (pos == -1)
            {
                pos = t.length() - 1;
            }

            String fullName = this.root + current + collected + dir;

            File file = new File(fullName);

            if (!file.exists())
            {
                return CWD_DIRECTORY_DOES_NOT_EXIST;
            }

            collected += dir + "/";
            t = t.substring(pos + 1);
        } while (!t.isEmpty());

        if (current.isEmpty())
        {
            current = collected.substring(1, collected.length() - 1);;
        }
        else
        {
            current += collected.substring(0, collected.length() - 1);
        }

        this.current = current;

        assert !this.current.endsWith("/");

        return CWD_OK;
    }


    public void listCurrentDir(DataOutputStream out)
        throws IOException
    {
        File dir = new File(this.getCurrentLocalDirectory());

        String[] dirs = dir.list();

        if (dirs == null)
        {
            return;
        }

        for (String name : dirs)
        {
            String pathname = this.root + this.current + "/" + name;
            
            File f = new File(pathname);

            Date date = new Date(f.lastModified());

            // TODO: Form data correctly!
            if (f.isDirectory())
            {
                out.writeBytes("drwxr-xr-x 2 106 109 0 " + DATE_FORMAT.format(date) + " ");
            }
            else
            {
                out.writeBytes("-rw-r--r-- 1 0 0 " + f.length() + " " + DATE_FORMAT.format(date) + " ");
            }

            out.write(to_utf8(name));
            out.writeBytes("\n");
        }
    }


    public boolean retrieveFile(String filename, DataOutputStream out)
    {
        String pathname = this.argumentToLocalFilename(filename);

        if (pathname == null)
        {
            return false;
        }

        File f = new File(pathname);

        if (!f.exists())
        {
            return false;
        }

        byte[] buffer = new byte[BUFFER_SIZE];

        try
        {
            FileInputStream in = new FileInputStream(f);

            in.skip(this.startPos);

            // TODO: To improve performance align reads of the buffer by 1024.

            int n;

            while ( (n = in.read(buffer, 0, BUFFER_SIZE)) > 0 )
            {
                out.write(buffer, 0, n);
            }

            in.close();
        }
        catch (IOException e)
        {
            //e.printStackTrace();

            return false;
        }
        finally
        {
            this.startPos = 0;
        }

        return true;
    }


    public void setStartPos(long startPos)
    {
        this.startPos = startPos;
    }


    public boolean delete(String filename)
    {
        return legacy ? deleteJ7(filename) : deleteJ8(filename);
    }


    private boolean deleteJ8(String filename)
    {
        boolean result = false;

        String pathname = this.argumentToLocalFilename(filename);

        if (pathname == null)
        {
            return false;
        }

        try
        {
            //System.out.println("pathname = " + pathname);
            result = Files.deleteIfExists(new File(pathname).toPath());
        }
        catch (IOException e)
        {
            //e.printStackTrace();
        }

        return result;
    }


    private boolean deleteJ7(String filename)
    {
        boolean result = false;

        String pathname = this.argumentToLocalFilename(filename);

        if (pathname == null)
        {
            return false;
        }

        File file = new File(pathname);
        result = file.delete();

        return result;
    }


    public boolean createDir(String dir)
    {
        String pathname = this.argumentToLocalFilename(dir);

        if (pathname == null)
        {
            return false;
        }

        File dirFile = new File(pathname);

        return dirFile.mkdir();
    }


    public long getSize(String filename)
    {
        String pathname = this.argumentToLocalFilename(filename);

        if (pathname == null)
        {
            // TODO: What should be returned?
            return 0;
        }

        File f = new File(pathname);

        return f.length();
    }


    public static byte[] to_utf8(String s)
    {
        byte[] bytes = UTF8.encode(s).array();

        int len = bytes.length - 1;

        while (len >= 0 && bytes[len] == 0)
        {
            --len;
        }

        return Arrays.copyOfRange(bytes, 0, len + 1);
    }


    public boolean storeFile(String filename, DataInputStream dataIn)
    {
        boolean result = false;

        String pathname = this.argumentToLocalFilename(filename);

        if (pathname == null)
        {
            return false;
        }

        try
        {
            FileOutputStream out = new FileOutputStream(new File(pathname));

            byte[] buffer = new byte[BUFFER_SIZE];

            int read;

            while ( (read = dataIn.read(buffer, 0, BUFFER_SIZE)) > 0 )
            {
                out.write(buffer, 0, read);
            }

            out.close();

            result = true;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return result;
    }


    public boolean exists(String filename)
    {
        String localFilename = this.argumentToLocalFilename(filename);

        if (localFilename == null)
        {
            return false;
        }

        File file = new File(localFilename);

        return file.exists();
    }


    public int rename(String src, String dst)
    {
        String localSrc = this.argumentToLocalFilename(src);
        String localDst = this.argumentToLocalFilename(dst);

        if (localSrc == null || localDst == null)
        {
            return 3;
        }

        File srcFile = new File(localSrc);
        File dstFile = new File(localDst);

        if (!srcFile.exists())
        {
            return 2;
        }

        // TODO: Check for destination?

        return srcFile.renameTo(dstFile) ? 0 : 1;
    }


    private String argumentToLocalFilename(String argument)
    {
        return legacy ? argumentToLocalFilenameJ7(argument) : argumentToLocalFilenameJ8(argument);
    }


    private String argumentToLocalFilenameJ8(String argument)
    {
        if (argument == null)
        {
            return null;
        }

        Path rootPath = Paths.get(this.root).normalize();

        Path candidatePath;

        if (argument.startsWith("/"))
        {
            candidatePath = Paths.get(argument.substring(1));
        }
        else
        {
            candidatePath = Paths.get(this.current);

            candidatePath = candidatePath.resolve(argument);
        }

        candidatePath = candidatePath.normalize();

        // Do not allow to go any higher than root!
        if (candidatePath.toString().startsWith(".."))
        {
            return null;
        }

        candidatePath = rootPath.resolve(candidatePath).normalize();

        // One more security check
        if (!candidatePath.startsWith(rootPath))
        {
            return null;
        }

        return candidatePath.toString().replaceAll("\\\\", "/");
    }


    private String argumentToLocalFilenameJ7(String argument)
    {
        // TODO: Sanitise input!

        if (argument == null)
        {
            return null;
        }

        if (argument.startsWith("/"))
        {
            return this.root + argument.substring(1);
        }
        else
        {
            return this.getCurrentLocalDirectory() + "/" + argument;
        }
    }
}
